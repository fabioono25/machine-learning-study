# Data Preprocessing Template

# Importing the libraries
import numpy as np               #mathematital tools
import matplotlib.pyplot as plt  #plot charts
import pandas as pdb             #manage datasets

# Importing the dataset
dataset = pdb.read_csv('Data.csv')
X = dataset.iloc[:, :-1].values #all the lines, all the columns except the last one
Y = dataset.iloc[:, 3].values #take the last column

#replace the missing data with the min value
#from sklearn.preprocessing import Imputer
#imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)
#imputer = imputer.fit(X[:, 1:3]) #taking indexes 1 and 2
#X[:, 1:3] = imputer.transform(X[:, 1:3])

# Encoding categorical data
#from sklearn.preprocessing import LabelEncoder, OneHotEncoder
#labelencoder_X = LabelEncoder()
#X[:, 0] = labelencoder_X.fit_transform(X[:,0])
#
#onehotencoder = OneHotEncoder(categorical_features= [0])
#X = onehotencoder.fit_transform(X).toarray()
#
#labelencoder_Y = LabelEncoder()
#Y = labelencoder_Y.fit_transform(Y)

#splitting the dataset into Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size = 0.2, random_state = 0) #20%


## Splitting the dataset into the Training set and Test set
#from sklearn.cross_validation import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

#from sklearn.model_selection import train_test_split 

# Feature Scaling
'''from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)'''
